<?php
class News_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	public function get_news($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('news');
			return $query->result_array();
		}

	$query = $this->db->get_where('news', array('slug' => $slug));
	return $query->row_array();
	}

	public function set_news()
	{
	$this->load->helper('url');

	$slug = url_title($this->input->post('title'), 'dash', TRUE);//url title is a helper that strips replaces spaces with dashes and cast as lower for slugs

	$data = array(
		'title' => $this->input->post('title'),//supposed sanitization of post data
		'slug' => $slug,
		'text' => $this->input->post('text')
	);

	return $this->db->insert('news', $data);
	}
}